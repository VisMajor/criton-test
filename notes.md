# Changes made
 - Changed self.pieces setup in `__init__` to use MAX_BOARD_WIDTH & MAX_BOARD_HEIGHT
 - Changed test_position_out_of_bounds_north_is_invalid() to assert with false - name of test indicated that was the intention
 - Changed parameter name from `pawn` to `piece` in ChessBoard.add() method, any piece can use it

# Notes for ChessBoard
 - is_legal_board_position() also uses MAX_BOARD_WIDTH & MAX_BOARD_HEIGHT, and checks for both positive and negative out-of-bounds.
 - could change is_legal_board_position() to do 4 checks instead of an `or` statement, but I found it easier to read to use `or`
 - added a coordinates_empty() to check if given coordinates are occupied by a piece or not. It also tries/catches negative coordinates
 - added 2 more tests to test for west & south out_of_bounds coordinates
 - added 2 more tests to test for negative x and y coordinates
 - add() must check for legal_position and empty coordinates in a specific order - not too robust. Test itself is asking for invalid placements' coordinates to be set to -1, -1, thus restricting some options
 - ChessBoard should also have a move_piece() method so it can update the position of the piece

# Notes for Pawn
 - move() differentiates between black and white pieces
 - stops pieces from going off the grid
 - moving the piece updates it's position on the chessboard

# Ideas for extension:
 - check if pawn movement is valid (e.g. cannot move backwards) - Done, cannot move backwards
 - use inheritance for repeated code in pieces - Done, implemented Piece superclass responsible for getters/setters/update coordinates
 - number_of_pawns_limited test is not really testing for the number of pawns on board
 - add() method in ChessBoard takes in PieceColor even when piece already has PieceColor - feels like unnecessary duplication
 - Could potentially put more shared behaviour into superclass.
