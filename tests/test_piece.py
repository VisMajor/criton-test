import unittest

import os
import sys
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, ".."))

from src.chess_board import ChessBoard
from src.piece import Piece
from src.piece_color import PieceColor

class TestPiece(unittest.TestCase):
    def setUp(self):
        super().setUp()

        self.chess_board = ChessBoard()
        self.piece = Piece(PieceColor.BLACK)

    def test_that_add_sets_x_coordinates(self):
        self.chess_board.add(self.piece, 6, 3, PieceColor.BLACK)
        assert self.piece.x_coordinate == 6

    def test_that_add_Sets_y_coordinate(self):
        self.chess_board.add(self.piece, 6, 3, PieceColor.BLACK)
        assert self.piece.y_coordinate == 3

if __name__ == '__main__':
    unittest.main()
