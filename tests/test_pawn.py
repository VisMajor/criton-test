import unittest

import os
import sys
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, ".."))

from src.chess_board import ChessBoard
from src.movement_type import MovementType
from src.pawn import Pawn
from src.piece import Piece
from src.piece_color import PieceColor


class TestPawn(unittest.TestCase):
    def setUp(self):
        super().setUp()

        self.chess_board = ChessBoard()
        self.pawn = Pawn(PieceColor.BLACK)

    def test_that_can_calculate_valid_move_coordinates_for_black_piece(self):
        self.chess_board.add(self.pawn, 6, 6, PieceColor.BLACK)
        valid_coordinates = self.pawn.calculate_valid_move_coordinates()
        print(self.pawn._piece_color)
        assert valid_coordinates == [(6, 5)]

    def test_that_pawn_has_piece_for_parentclass(self):
        assert issubclass(Pawn, Piece) == True

    def test_that_move_illegal_coords_right_does_not_move(self):
        self.chess_board.add(self.pawn, 6, 3, PieceColor.BLACK)
        self.pawn.move(MovementType.MOVE, 7, 3)

        assert self.pawn.x_coordinate == 6
        assert self.pawn.y_coordinate == 3

    def test_that_move_illegal_coords_left_does_not_move(self):
        self.chess_board.add(self.pawn, 6, 3, PieceColor.BLACK)
        self.pawn.move(MovementType.MOVE, 4, 3)

        assert self.pawn.x_coordinate == 6
        assert self.pawn.y_coordinate == 3

    def test_that_move_to_legal_coords_forward_does_move(self):
        self.chess_board.add(self.pawn, 6, 3, PieceColor.BLACK)
        self.pawn.move(MovementType.MOVE, 6, 2)

        assert self.pawn.x_coordinate == 6
        assert self.pawn.y_coordinate == 2

    def test_that_move_to_out_of_bounds_coordinates_does_not_move(self):
        self.chess_board.add(self.pawn, 6, 0, PieceColor.BLACK)
        self.pawn.move(MovementType.MOVE, 6, -1)

        assert self.pawn.x_coordinate == 6
        assert self.pawn.y_coordinate == 0

    def test_that_can_check_for_valid_moves_returns_true(self):
        self.chess_board.add(self.pawn, 6, 2, PieceColor.BLACK)
        proposed_coordinates = (6,1)
        valid_coordinates = self.pawn.calculate_valid_move_coordinates()
        result = self.pawn.is_valid_move(proposed_coordinates, valid_coordinates)
        assert result == True

    def test_that_can_check_for_valid_moves_returns_false(self):
        self.chess_board.add(self.pawn, 6, 2, PieceColor.BLACK)
        proposed_coordinates = (6,0)
        valid_coordinates = self.pawn.calculate_valid_move_coordinates()
        result = self.pawn.is_valid_move(proposed_coordinates, valid_coordinates)
        assert result == False

if __name__ == '__main__':
    unittest.main()
