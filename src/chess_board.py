from src.movement_type import MovementType

class ChessBoard:
    MAX_BOARD_WIDTH = 7
    MAX_BOARD_HEIGHT = 7

    def __init__(self):
        self.pieces = [[None] * (self.MAX_BOARD_WIDTH + 1) for _ in range(self.MAX_BOARD_HEIGHT + 1)]

    def add(self, piece, x_coordinate, y_coordinate, piece_color):
        if( self.is_legal_board_position(x_coordinate, y_coordinate) and self.coordinates_empty(x_coordinate, y_coordinate)  ):
            self.pieces[x_coordinate][y_coordinate] = piece
            piece.update_coordinates(self, x_coordinate, y_coordinate)
        else:
            piece.update_coordinates(self, -1, -1)

    def move_with_piece(self, piece, new_x, new_y):
        if(self.is_legal_board_position(new_x, new_y) and self.coordinates_empty(new_x, new_y)):
            self.pieces[piece.x_coordinate][piece.y_coordinate] = None
            self.add(piece, new_x, new_y, piece._piece_color)
            piece.move(MovementType.MOVE, new_x, new_y)

    def is_legal_board_position(self, x_coordinate, y_coordinate):
        if(x_coordinate > self.MAX_BOARD_HEIGHT or x_coordinate < 0):
            return False
        if(y_coordinate > self.MAX_BOARD_WIDTH or y_coordinate < 0):
            return False
        return True

    def coordinates_empty(self, x_coordinate, y_coordinate):
        try:
            if x_coordinate < 0 or y_coordinate < 0:
                raise ValueError
        except ValueError:
            return "coordinates cannot be negative number"

        return self.pieces[x_coordinate][y_coordinate] == None
