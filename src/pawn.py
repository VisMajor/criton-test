from src.piece import Piece
from src.piece_color import PieceColor
from src.movement_type import MovementType

class Pawn(Piece):
    def __init__(self, piece_color):
        super().__init__(piece_color)

    def move(self, movement_type, new_x, new_y):
        if(movement_type == MovementType.MOVE):
            valid_coordinates = self.calculate_valid_move_coordinates()
            proposed_coordinates = (new_x, new_y)
            if(self.is_valid_move(proposed_coordinates, valid_coordinates)):
                self.update_coordinates(self._chess_board, new_x, new_y)

    def is_valid_move(self, proposed_coordinates, valid_coordinates):
        if(proposed_coordinates in valid_coordinates) and (self._chess_board.is_legal_board_position(proposed_coordinates[0], proposed_coordinates[1])):
            return True
        return False

    def calculate_valid_move_coordinates(self):
        valid_coordinates = []
        if(self.piece_color == PieceColor.BLACK):
            valid_coordinates.append((self.x_coordinate, self.y_coordinate - 1))
        if(self.piece_color == PieceColor.WHITE):
            valid_coordinates.append((self.x_coordinate, self.y_coordinate + 1))
        return valid_coordinates

    def __unicode__(self):
        return 'Current X: {}\nCurrent Y: {}\nPiece Color: {}'.format(
            self.x_coordinate, self.y_coordinate, self.piece_color
        )
